**JSON generator template**

    [
      '{{repeat(20, 20)}}',
      {
        id: '{{index(1)}}',
        name: '{{company()}}',
        description: '{{lorem(15, "sentences")}}',
        price: '{{integer(1000, 15000)}}' ,
        start: '{{date(new Date(1970, 0, 1),new Date(),"ISODateTime")}}.000Z'
      }
    ]
